#!/bin/bash -x

if [[ -z $(ls /var/lib/mysql/) ]]; then
    # we are installing for the first time
    rsync -a /var/lib/mysql1/ /var/lib/mysql/
    rsync -a /etc/mysql1/ /etc/mysql/

    # allow requests from the network
    sed -i /etc/mysql/mariadb.conf.d/50-server.cnf \
        -e 's/^bind-address/#bind-address/'
fi

# make sure the ownership is correct
chown -R mysql: /var/lib/mysql
chown -R mysql: /var/log/mysql

# restart
systemctl enable mariadb
systemctl start mariadb

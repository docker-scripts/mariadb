include(bookworm)

RUN <<EOF
  apt update
  apt upgrade --yes
  apt install --yes \
      curl ca-certificates apt-transport-https

  # add mariadb repository
  curl -LO https://r.mariadb.com/downloads/mariadb_repo_setup
  chmod +x mariadb_repo_setup
  ./mariadb_repo_setup --mariadb-server-version="mariadb-10.11.7" --skip-maxscale
  rm mariadb_repo_setup

  # install mariadb
  apt install --yes \
      mariadb-server \
      mariadb-client \
      mariadb-backup \
      mariadb-plugin-provider-bzip2 \
      mariadb-plugin-provider-lz4 \
      mariadb-plugin-provider-lzma \
      mariadb-plugin-provider-lzo \
      mariadb-plugin-provider-snappy

  # backup directories that will be mounted
  systemctl stop mariadb
  systemctl disable mariadb
  cp -a /var/lib/mysql /var/lib/mysql1
  cp -a /etc/mysql /etc/mysql1
EOF

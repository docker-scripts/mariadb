rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p conf.d data dblogs
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/conf.d,dst=/etc/mysql/mariadb.conf.d \
        --mount type=bind,src=$(pwd)/data,dst=/var/lib/mysql \
        --mount type=bind,src=$(pwd)/dblogs,dst=/var/log/mysql

    # make the command 'mariadb' global
    mkdir -p $DSDIR/cmd/
    cp $APPDIR/cmd/mariadb.sh $DSDIR/cmd/
}
